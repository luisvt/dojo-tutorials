define(['dojo/dom'], function(dom) {
    var oldText;
    
    return {
        setText: function(id, text) {
            //$('"#' + id + '"').something()
            var node = dom.byId(id);
            oldText = node.innerHTML;
            node.innerHTML = text;
        },
        
        restoreText: function(id) {
            var node = dom.byId(id);
            node.innerHTML = oldText;
            delete oldText;
        }
    };
});

