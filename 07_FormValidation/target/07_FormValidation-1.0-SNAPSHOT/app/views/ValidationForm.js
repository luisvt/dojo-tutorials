define([
    "dojo/_base/declare",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/text!./ValidationForm.html",
    
    'dijit/form/Form',
    'dojox/layout/TableContainer',
    'dijit/form/ValidationTextBox',
    'dojox/validate/web'

], function(declare, _WidgetsInTemplateMixin, template, Form) {
    return declare([Form, _WidgetsInTemplateMixin], {
        templateString: template
    });
});


